class Puissance4 {
    constructor(WinDetector) {
        this.boardElement = document.querySelector("#game");
        this.verticalCells = [];
        this.players = ["rouge", "bleu"];
        this.turns = 1;

        this.boardElement.querySelectorAll(".column").forEach(column => {
            let cells = [...column.querySelectorAll(".cell")];
            this.verticalCells.push(cells);
        });

        this.winDetector = new WinDetector(this.verticalCells);
    }

    start() {
        this.playerTurn = 0;
        this.time = + new Date();

        this.boardElement.addEventListener("click", e => {
            let cell = e.target;

            if(!cell.classList.contains("cell") || cell.classList.contains("filled")) {
                return;
            }
            this.insertColumn(cell.parentNode.dataset.index);
            const winningCells = this.winDetector.detect(this.verticalCells);
            if(winningCells) {
                Puissance4.highlight(winningCells);
                this.end();
            }
            this.nextTurn();
        });
        this.updateHover();
    }

    insertColumn(index) {
        let cells = this.verticalCells[index];

        for(let i=cells.length-1; i >= 0; i--) {
            if(!cells[i].classList.contains("filled")) {
                cells[i].className = "cell filled p"+this.playerTurn;
                break;
            }
        }
    }

    nextTurn() {
        this.playerTurn = this.playerTurn === 1 ? 0 : 1;
        this.updateHover();
        this.turns++;
    }

    updateHover() {
        this.boardElement.className = "hover p" + this.playerTurn;
    }

    end() {
        let winningScreen = document.createElement("div");
        let elapsedSeconds = Math.floor((+ new Date() - this.time) / 1000);

        winningScreen.id = "winningScreen";
        winningScreen.innerHTML = `
                <div class="content">
                    <h1>Bravo Joueur ${this.players[this.playerTurn]}</h1>
                    <p>Victoire en ${elapsedSeconds} secondes et ${this.turns} tours.</p>
                    <button onclick="game.restart()">Recommencer</button>
                </div>
            `;
        this.boardElement.append(winningScreen);
    }

    restart() {
        let winScreen = this.boardElement.querySelector("#winningScreen");
        this.boardElement.removeChild(winScreen);

        this.verticalCells.forEach(cells => {
            cells.forEach(cell => cell.className = "cell");
        });
        this.time = + new Date();
        this.turns = 1;
    }

    static highlight(cells) {
        cells.forEach( cell => cell.classList.add("highlight") );
    }
}

export { Puissance4 };