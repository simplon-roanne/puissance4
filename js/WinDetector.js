class WinDetector {
    constructor(verticalCells) {
        this.verticalCells = verticalCells;
        this.horizontalCells = this.getHorizontalCells();
        this.diagonalCells = this.getDiagonalCells();
    }

    getHorizontalCells() {
        let horizontalCells = [];
        for(let columnIndex = 0; columnIndex < this.verticalCells.length ; columnIndex++) {
            let cells = this.verticalCells[columnIndex];
            for(let i = 0; i < cells.length ; i++) {
                if(typeof horizontalCells[i] === "undefined") {
                    horizontalCells[i] = [];
                }
                horizontalCells[i].push(cells[i]);
            }
        }

        return horizontalCells;
    }

    getDiagonalCells() {
        return [
            this.getDiagonalCellsLineRight(0,3),
            this.getDiagonalCellsLineRight(0,2),
            this.getDiagonalCellsLineRight(0,1),
            this.getDiagonalCellsLineRight(0,0),
            this.getDiagonalCellsLineRight(1,0),
            this.getDiagonalCellsLineRight(2,0),
            this.getDiagonalCellsLineLeft(5,3),
            this.getDiagonalCellsLineLeft(5,2),
            this.getDiagonalCellsLineLeft(5,1),
            this.getDiagonalCellsLineLeft(5,0),
            this.getDiagonalCellsLineLeft(4,0),
            this.getDiagonalCellsLineLeft(3,0),
        ];
    }

    getDiagonalCellsLineRight(x,y) {
        return this.getDiagonalCellsLine(x,y,true);
    }

    getDiagonalCellsLineLeft(x,y) {
        return this.getDiagonalCellsLine(x,y,false);
    }

    getDiagonalCellsLine(x,y, right) {
        let diagonalCells = [];
        const vCells = this.verticalCells;

        const defined = value =>  typeof value !== "undefined";
        const cellExists = (x,y) => defined(vCells[x]) && defined(vCells[x][y]);


        while(cellExists(x, y)) {

            diagonalCells.push(vCells[x][y]);

            if(right) {
                x++;
            }
            else {
                x--;
            }
            y++;
        }

        return diagonalCells;
    }

    detect() {
        const winningCells = [
            ...WinDetector.getWinningCells(this.verticalCells),
            ...WinDetector.getWinningCells(this.horizontalCells),
            ...WinDetector.getWinningCells(this.diagonalCells),
        ];

        return winningCells.length ? winningCells : false;
    }

    static getWinningCells(cells) {
        // Vertical
        for(let i = 0; i < cells.length ; i++) {
            const winningCells = WinDetector.testWin(cells[i]);
            if(winningCells) {
                return winningCells;
            }
        }
        return [];
    }

    static testWin(cells) {
        let winningCells = [cells[0]];
        let previousCellColor;


        for(let i = 0; i < cells.length ; i++) {
            let currentCellColor = WinDetector.getCellColor(cells[i]);
            if(previousCellColor !== null && previousCellColor === currentCellColor) {
                winningCells.push(cells[i]);
            }
            else {
                winningCells = [cells[i]]
            }
            if(winningCells.length === 4) {
                return winningCells;
            }
            previousCellColor = currentCellColor;
        }

        return false;
    }

    static getCellColor(cell) {
        let color = null;
        if(cell.classList.contains("filled")) {
            color = cell.classList.contains("p0") ? 0 : 1;
        }
        return color;
    }
}

export { WinDetector };